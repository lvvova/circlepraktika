﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyManager : MonoBehaviour
{
    void OnEnable()
    {
        PieceHolder.circleIsFull += ManageDestroying;
    }
    void OnDisable()
    {
        PieceHolder.circleIsFull -= ManageDestroying;
    }

    void ManageDestroying(PieceHolder pieceHolder)
    {
        var circleArray = GetComponent<GameManager>().ReturnPiceceHolders();
        var listPos = circleArray.IndexOf(pieceHolder);

        List<PieceHolder> DestroyCircleList = new List<PieceHolder>();
        for (int i = -1; i < 3 - 1; i++)
        {
            DestroyCircleList.Add(circleArray[returnMoreOrLessSix(listPos + i)]);
        }
        foreach (var item in DestroyCircleList)
        {
            DestroyPieces(item);
        }
    }

    void DestroyPieces(PieceHolder pieceHolder)
    {
        pieceHolder.pieceData = new PieceData();
        for (int i = 0; i < pieceHolder.GetPieceContainer().childCount; i++)
        {
            var obj = pieceHolder.GetPieceContainer().GetChild(i);
            Destroy(obj.gameObject);
        }
        pieceHolder.ReactOnAction();
    }

    int returnMoreOrLessSix(int val)
    {
        if (val < 0)
            return val + 6;
        else if (val > 5)
            return val - 6;
        else
            return val;
    }
}
