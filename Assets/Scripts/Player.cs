﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] Circle baseHolder;
    Camera cachedCamera;
    int circleLayer;

    bool readyToPushPiece;

    void OnEnable()
    {
        PieceHolder.attemptToSendCompleted += () => { readyToPushPiece = true; };
    }

    void OnDisable()
    {
        PieceHolder.attemptToSendCompleted = null;
    }

    void Awake()
    {
        readyToPushPiece = true;
        cachedCamera = Camera.main;
        circleLayer = 1 << 8;
    }

    void Update()
    {
        Vector2 mousePosition = cachedCamera.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            if (readyToPushPiece)
            {
                RaycastHit2D hit = Physics2D.Raycast(mousePosition, Vector2.zero, Mathf.Infinity, circleLayer);
                if (hit.transform != null)
                {
                    readyToPushPiece = false;
                    hit.transform.gameObject.GetComponent<PieceHolder>().CheckForInsertion(baseHolder.pieceData);
                }
            }
        }
    }
}
