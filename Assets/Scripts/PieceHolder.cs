﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class PieceHolder : Circle
{
    public static System.Action pieceProcessedSuccessful;
    public static System.Action attemptToSendCompleted;
    public static System.Action<PieceHolder> circleIsFull;
    
    [SerializeField] ParticleSystem reactEffect;

    public void CheckForInsertion(PieceData takenPiece)
    {
        ReactOnAction();
        if (checkSame(takenPiece, pieceData))
        {
            ReturnBack(takenPiece);
        }
        else
        {
            IncertPieces(takenPiece);
            pieceProcessedSuccessful?.Invoke();
        }
        checkFullCircle();
        attemptToSendCompleted?.Invoke();
    }

    private void checkFullCircle()
    {
        if (System.Array.TrueForAll(pieceData.pieces, x => x == 1))
            {
                circleIsFull?.Invoke(this);
            }
    }

    private void ReturnBack(PieceData takenPiece)
    {
        Debug.LogWarning("NotImplementedException");
    }

    public void ReactOnAction()
    {
        reactEffect.Play();
        Sequence mySeq = DOTween.Sequence();
        mySeq.Append(transform.DOScale(0.85f, 0.1f));
        mySeq.Append(transform.DOScale(1f, 0.5f));
    }

    bool checkSame(PieceData firstPieces, PieceData secondPieces)
    {
        bool haveSameSegment = false;
        for (int i = 0; i < firstPieces.pieces.Length; i++)
        {
            if (firstPieces.pieces[i] == 1)
            {
                if (firstPieces.pieces[i] == secondPieces.pieces[i])
                {
                    haveSameSegment = true;
                    break;
                }
            }
        }
        return haveSameSegment;
    }
    public Transform GetPieceContainer() => pieceContainer;
}
