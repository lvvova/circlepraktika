﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PieceData
{
    public int[] pieces;
    public List<GameObject> visualPices;

    public PieceData()
    {
        pieces = new int[6];
        visualPices = new List<GameObject>();
    }
}
