﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] List<PieceHolder> pieceHolders;
    [SerializeField] Circle baseHolder;
    [SerializeField] List<float> pieceDropCountChances;
    [SerializeField] GameObject piece;

    void OnEnable()
    {
        PieceHolder.pieceProcessedSuccessful += StartNewGameCycle;
    }
    
    void OnDisable()
    {
        PieceHolder.pieceProcessedSuccessful -= StartNewGameCycle;
    }

    void Start()
    {
        StartNewGameCycle();
    }

    private void StartNewGameCycle()
    {
        baseHolder.pieceData = generatePieceData();
        baseHolder.pieceData.visualPices = generatePieceView(baseHolder.pieceData);
        baseHolder.IncertPieces(baseHolder.pieceData);
        CheckGameOver();
    }

    private PieceData generatePieceData()
    {
        PieceData pieceData = new PieceData();
        for (int i = 0; i < pieceDropCountChances.Count; i++)
        {
            if (Random.Range(0, 1f) < pieceDropCountChances[i])
            {
                var randomPoint = Random.Range(0, pieceData.pieces.Length);
                pieceData.pieces[randomPoint] = 1;
                for (int j = 0; j < Mathf.Abs(i - pieceDropCountChances.Count); j++)
                {
                    pieceData.pieces[returnMoreOrLessSix(randomPoint + j)] = 1;
                }
                break;
            }
        }
        return pieceData;
    }

    private List<GameObject> generatePieceView(PieceData pieceData)
    {
        List<GameObject> pieceList = new List<GameObject>();
        for (int i = 0; i < pieceData.pieces.Length; i++)
        {
            if (pieceData.pieces[i] == 1)
            {
                var spawnedPiece = Instantiate(piece, transform.position, Quaternion.identity, null) as GameObject;
                spawnedPiece.transform.eulerAngles = new Vector3(0, 0, i * 60);
                pieceList.Add(spawnedPiece);
            }
        }
        return pieceList;
    }
    void CheckGameOver()
    {
        var currentPiece = baseHolder.pieceData;
        int cantSendCounter = 0;
        foreach (var item in pieceHolders)
        {
            if (checkSame(item.pieceData, currentPiece))
            {
                cantSendCounter++;
            }
        }
        if (cantSendCounter == 6)
        {
            GameOver();
        }
    }

    private void GameOver()
    {
        Debug.Log("Game over, Restarting");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    bool checkSame(PieceData firstPieces, PieceData secondPieces)
    {
        bool haveSameSegment = false;
        for (int i = 0; i < firstPieces.pieces.Length; i++)
        {
            if (firstPieces.pieces[i] == 1)
            {
                if (firstPieces.pieces[i] == secondPieces.pieces[i])
                {
                    haveSameSegment = true;
                    break;
                }
            }
        }
        return haveSameSegment;
    }

    public List<PieceHolder> ReturnPiceceHolders() => pieceHolders; 

    int returnMoreOrLessSix(int val)
    {
        if (val < 0)
            return val + 6;
        else if (val > 5)
            return val - 6;
        else
            return val;
    }
}
